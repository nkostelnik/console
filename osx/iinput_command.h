#ifndef IINPUT_COMMAND_H_SHEK2EZJ
#define IINPUT_COMMAND_H_SHEK2EZJ

#include <SDL/SDL.h>

#include <iostream>

class IInputCommand {
  
public:
  
  virtual void task(void* data) {
    this->execute(*static_cast<SDL_Event*>(data)); 
  }

  virtual void execute(const SDL_Event& event) = 0;
  
};

#endif /* end of include guard: IINPUT_COMMAND_H_SHEK2EZJ */
