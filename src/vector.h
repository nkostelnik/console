#ifndef __VECTOR
#define __VECTOR

#include <iostream>

class Vector {
  
public:

  Vector() {
    x, y , z, w = 0.0f;
  }

  Vector(float x_, float y_, float z_, float w_ = 0.0f) 
    : x(x_), y(y_), z(z_), w(w_) { }
  
  float x;
  float y;
  float z;
  float w;
};

std::ostream& operator << (std::ostream& stream, const Vector& m);

#endif
