#ifndef __GEOMETRY
#define __GEOMETRY

#include "vector.h"
#include "matrix.h"

#include "log.h"

#include <SDL/SDL_opengl.h>

#ifdef __APPLE__

#include <OpenGL/OpenGL.h>

#else

#include <GL/gl.h>

#endif

#include <deque>
#include <sstream>
#include <string.h>

class Color { 
  
  public:
  
  Color(float r_, float g_, float b_, float a_)
    : r(r_), g(g_), b(b_), a(a_) { }
    
  float r;
  float g;
  float b;
  float a;
  
};

class Material {
  
  public:
  
  Material(const Color& color)
  { 
    diffuse_[0] = color.r;
    diffuse_[1] = color.g;
    diffuse_[2] = color.b;
    diffuse_[3] = color.a;
  }
  
  void render() {
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_);
  }
  
  private:
  
  float diffuse_[4];

};
  
class Geometry {
  
  typedef std::deque<Material> MaterialDeque;
  typedef std::deque<float> FloatDeque;
  
  public:
  
  Geometry(float* vertices, float* normals, unsigned int vertex_count) 
    : vertices_(vertices)
    , normals_(normals)
    , vertex_count_(vertex_count) { } 
  
  void render() {
    glPushAttrib(GL_LIGHTING_BIT);
    glPushMatrix();
    
    set_materials();
    set_geometry();
    
    glPopMatrix();
    glPopAttrib();
  }
  
  void scale(const Vector& scale) { 
    scale_ = scale;
  }
  
  void translate(const Vector& translation) {
    position_.x += translation.x;
    position_.y += translation.y;
    position_.z += translation.z;
  }
  
  void rotate(const Matrix& rotation) {
    rotation_ = rotation;
  }
  
  void add_material(const Material& material) {
    materials_.push_back(material);
  }
  
  private:
  
  void set_materials() {
    for (MaterialDeque::iterator i = materials_.begin(); i != materials_.end(); ++i) {
      (*i).render();
    }
  }
  
  void set_geometry() {
    glScalef(scale_.x, scale_.y, scale_.z);
    glTranslatef(position_.x, position_.y, position_.z);
    
    float* array = new float[16];
    rotation_.array(array);
    //glMultMatrixf(array);
    delete[] array;

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    
    glVertexPointer(3, GL_FLOAT, 0, vertices_);
    glNormalPointer(GL_FLOAT, 0, normals_);
        
    glDrawArrays(GL_TRIANGLES, 0, vertex_count_);

    glDisableClientState(GL_NORMAL_ARRAY);     
    glDisableClientState(GL_VERTEX_ARRAY);
  }
  
  MaterialDeque materials_;
  Vector scale_;
  Vector position_;
  Matrix rotation_;
  
  float* vertices_;
  unsigned int vertex_count_;
  float* normals_;
  
};


#endif
