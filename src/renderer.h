#ifndef __RENDERER
#define __RENDERER

#include <deque>    

class Model;

class Renderer {
  
  typedef std::deque<Model*> ModelDeque;

public:

  void init(int width, int height);
    
  void add_to_scene(Model* model);
  
  void render();  
  
private:
  
  ModelDeque models_;

};

#endif
