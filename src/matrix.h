#ifndef __MATRIX
#define __MATRIX

#include "vector.h"
#include "degrees.h"

#include <string.h>
#include <iostream>

class Matrix {
  
  public:
  
  Matrix() {
  }
  
  Matrix(const Vector& row1, const Vector& row2, const Vector& row3)
    : m11(row1.x), m12(row1.y), m13(row1.z), m14(row1.w)
    , m21(row2.x), m22(row2.y), m23(row2.z), m24(row2.w)
    , m31(row3.x), m32(row3.y), m33(row3.z), m34(row3.w)
    , m41(0), m42(0), m43(0), m44(1) { }
    
  Matrix(const Vector& axis, Degrees degrees);
  
  Matrix(const Vector& axis, float radians);
          
  void array(float* array) {                      
    float matrix[] = {m11, m21, m31, m41,
                      m12, m22, m32, m42,
                      m13, m23, m33, m43,
                      m14, m24, m34, m44};
                                      
    memcpy(array, matrix, sizeof(float) * 16);
  };
  
  float m11, m12, m13, m14, 
        m21, m22, m23, m24, 
        m31, m32, m33, m34,
        m41, m42, m43, m44;
  
  Matrix operator * (const Matrix& m) const;
    
  private:
  
  void init(const Vector& axis, float radians);
};

std::ostream& operator << (std::ostream& stream, const Matrix& m);

Vector operator * (const Vector& vector, const Matrix& matrix);

#endif
