#include <queue>
#include <pthread.h>

struct ITask {
  virtual void run() = 0;
};

template <class T>
struct MemberTask : public ITask {
  MemberTask(T* target_, void(T::*task_function_)(void*), void* args_)
  {
    target = target_;
    task_function = task_function_;
    args = args_;
  }
  
  void run() {
    printf("hello");
    (target->*task_function)(args);
  }
  
  T* target;
  void(T::*task_function)(void*);
  void* args;
};

struct Task : public ITask {
  typedef void*(func)(void*);
  
  Task(func* task_function_, void* args_)
  {
    task_function = task_function_;
    args = args_;
  }
  
  void run() {
    (*task_function)(args);
  }
    
  func* task_function;
  void* args;
};

class TaskQueue {

  typedef void*(func)(void*);
  typedef std::queue<ITask*> TaskDeque;
  typedef std::deque<pthread_t*> ThreadDeque;
  
  public:
  
  static void stop_tasks();
  
  static void queue_task(func task_function, void* args);
  
  template <class T>
  static void queue_task(T* target, void(T::*task_function)(void*), void* args) {
    MemberTask<T>* task = new MemberTask<T>(target, task_function, args);
    pthread_mutex_lock(&task_mutex_);
    queued_tasks_.push(task);
    pthread_mutex_unlock(&task_mutex_);    
  }
    
  static void init();
  
  private:
  
  static void* thread_worker(void* args);
  static ITask* next_task();
  static pthread_mutex_t task_mutex_;
  
  static pthread_t* spawn_thread(void*(*thread_function)(void*), void* args);
  static TaskDeque queued_tasks_;
  
  static bool working_;
  static pthread_mutex_t working_mutex_;
  static bool working();
  
  static ThreadDeque threads_;
};
