#ifndef LINUX_INPUT_H_ZF38AZU3
#define LINUX_INPUT_H_ZF38AZU3

#include <linux/joystick.h>

void Input::init() {
  if ((joystick_file_ = open("/dev/input/js0", O_RDONLY)) == -1) {
    std::cerr << "cannot find gamepad" << std::endl;
  }
  
  int joystick_name_size = 80;
  char joystick_name[joystick_name_size];
  int joystick_axis, joystick_buttons = 0;
  
  ioctl(joystick_file_, JSIOCGAXES, &joystick_axis);
  ioctl(joystick_file_, JSIOCGBUTTONS, &joystick_buttons);
  ioctl(joystick_file_, JSIOCGNAME(joystick_name_size), &joystick_name);
  
  fcntl(joystick_file_, F_SETFL, O_NONBLOCK);
  
  std::clog << "Found " << joystick_name << " with " << joystick_axis << 
  " axis and " << joystick_buttons << " buttons" << std::endl;
}

void Input::destroy() {
  close(joystick_file_);
}

void Input::update() {
  struct js_event joystick_event;
  
  read(joystick_file_, &joystick_event, sizeof(struct js_event));
  
  switch (joystick_event.type & ~JS_EVENT_INIT)
  {
    case JS_EVENT_AXIS:
      axis[joystick_event.number] = joystick_event.value;
      //std::clog << axis[joystick_event.number] << std::endl;
      break;
    case JS_EVENT_BUTTON:
      buttons[joystick_event.number] = joystick_event.value;
      //std::clog << joystick_event.number << std::endl;
      //std::clog << "button " << buttons[joystick_event.number] << std::endl;
      break;
  }
}

#endif /* end of include guard: LINUX_INPUT_H_ZF38AZU3 */
