#ifndef __MATHS
#define __MATHS

class Maths {
  
public:
  
  template<class T>
  static T square(T i) {
    return i * i;
  }
  
};

#endif
