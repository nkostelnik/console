bool quit = false;

#include "log.h"
#include "renderer.h"
#include "input.h"
#include "task_queue.h"
#include "game_state.h"

#include "dae_file.h"
#include "model.hpp"

#include <SDL/SDL.h>

Input input;
TaskQueue task_queue;
Renderer renderer;

void* load_data(void* data) {
  DAEFile* dae = new DAEFile();
  char* path = (char*)data;
  dae->load(path);  
  Model* model = new Model();
  model->load(dae);
  renderer.add_to_scene(model);
  return 0;
}

void* update_thread(void* data) {
  while(!quit) {
    
  }
  return 0;
}

int main(int argc, char *argv[]) {
  SDL_Init (SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
  
  GameState::init();

  TaskQueue::init();
  input.init();
  renderer.init(640, 480);
  
  //TaskQueue::queue_task(&update_thread, 0);
  //TaskQueue::queue_task(&load_data, (char*)"/Users/nk/Development/console/data/all.dae");
  //TaskQueue::queue_task(&load_data, (char*)"/Users/nk/Development/console/data/u.dae");
  //TaskQueue::queue_task(&load_data, (char*)"/Users/nk/Development/console/data/flick.dae");
  //TaskQueue::queue_task(&load_data, (char*)"/Users/nk/Development/console/data/cube.dae");
  TaskQueue::queue_task(&load_data, (char*)"/Users/nk/Development/console/data/face.dae");
  
  while(GameState::instance()->running()) {    
    input.update();
    renderer.render();
  }
  
  input.destroy();
  TaskQueue::stop_tasks();
  
  return 0;
}