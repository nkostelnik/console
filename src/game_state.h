#ifndef GAME_STATE_H_X027K0DQ
#define GAME_STATE_H_X027K0DQ

  class GameState {
    
  public:
    
    GameState()
      : running_(true) { }
      
    static void init();
    static GameState* instance();
    static void destroy();
    
    inline void quit() { running_ = false; };
    inline bool running() { return running_; };
    
  private:
    
    static GameState* instance_;
    
    bool running_;
    
  };

#endif /* end of include guard: GAME_STATE_H_X027K0DQ */
