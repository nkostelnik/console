#include "vector.h"

std::ostream& operator << (std::ostream& stream, const Vector& v) {
  stream << v.x << " " << v.y << " " << v.z;
  return stream;
}