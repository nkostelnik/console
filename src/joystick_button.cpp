#include "joystick_button.h"

#include "game_state.h"

enum PS3_BUTTONS {
  PS_BUTTON = 16
};

void JoystickButton::execute(const SDL_Event& event) {
  if (event.jbutton.button == PS_BUTTON) {
    GameState::instance()->quit();
  }
}