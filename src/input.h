#include "iinput.h"

#include "iinput_command.h"

#include <map>

class Input {
  
  typedef std::map<int, IInputCommand*> CommandMap;

  public:
  
  void init();
  
  void destroy();
  
  void update();
  
  private:
  
  int joystick_file_;
  int buttons[11];
  int axis[8];

  CommandMap commands_;
  
};
