#include "renderer.h"
#include "model.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GLUT/GLUT.h>

void Renderer::init(int width, int height) {
  SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, 1);
  SDL_SetVideoMode (width, height, 0, SDL_OPENGL);

  glViewport(0, 0, (GLsizei) width, (GLsizei) height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0, (GLfloat) width/(GLfloat) height, 0.1, 1000000.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt (0.0, 10.0, 20.0, 
            0.0, 0.0,  0.0, 
            0.0, 1.0,  0.0);
            
  glClearColor(1.00, 0.0, 0.0, 1.0);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
}

void Renderer::render() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  static float angle = 0.1f;
  
  glRotatef(angle, 0, 1, 0);
  glPushMatrix();
   
   for (ModelDeque::iterator i = models_.begin(); i != models_.end(); ++i) {
     (*i)->render();
   }
     
   glPopMatrix();
    
  SDL_GL_SwapBuffers();
  glutSwapBuffers();
}

void Renderer::add_to_scene(Model* model) {
  static pthread_mutex_t mutexsum_;
  pthread_mutex_lock (&mutexsum_);
  models_.push_back(model);
  pthread_mutex_unlock (&mutexsum_);
}
