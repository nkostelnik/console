#ifndef KEY_EVENT_H_F1WJ925
#define KEY_EVENT_H_F1WJ925

class SDL_Event;

class KeyEvent {
  
public:
  
  KeyEvent(SDL_Event* event)
    : event_(event) { };
    
private:
    
  SDL_Event* event_;

  
};

#endif /* end of include guard: KEY_EVENT_H_F1WJ925 */
