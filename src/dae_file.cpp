#include "dae_file.h"
#include "log.h"
#include "string_utils.hpp"

DAEFile::DAEVisualSceneDeque DAEFile::scenes() {
  TiXmlElement* element = document_.RootElement()->FirstChild("library_visual_scenes")->ToElement();
  for(TiXmlNode* child = element->FirstChild(); child != 0; child = child->NextSibling()) {
    DAEVisualScene scene(child->ToElement(), &document_);
    scenes_.push_back(scene);
  }
  return scenes_;
}

DAEVisualScene::DAENodeDeque DAEVisualScene::nodes() {
  DAENodeDeque nodes;
  for(TiXmlNode* child = element_->FirstChild("node"); child != 0; child = child->NextSibling("node")) {
    DAENode node(child->ToElement(), document_);
    nodes.push_back(node);
  }
  return nodes;
}

float DAENode::vector_index(const std::string& key, int index) {
  std::string text = element_
        ->FirstChild(key.c_str())
        ->ToElement()
        ->GetText();
        
  StringUtils::StringVector raw = StringUtils::split(text, " ");
  return StringUtils::s2f(raw)[index];
}

float DAENode::vector_index(const std::string& key, const std::string& sid, int index) {
  for (TiXmlNode* node = element_->FirstChild(key.c_str()); 
    node; node = node->NextSibling()) {
    if (node->ToElement()->Attribute("sid") == sid) {
      std::string text = node->ToElement()->GetText();
      StringUtils::StringVector raw = StringUtils::split(text, " ");
      return StringUtils::s2f(raw)[index];
    }
  }
  
  LOG("unable to find requested element", key.c_str());
  return 0;
}

DAEVector DAENode::scale() {
  return DAEVector(vector_index("scale",0),
                   vector_index("scale",1),
                   vector_index("scale",2));
}

DAEVector DAENode::translation() {
  return DAEVector(vector_index("translate",0),
                   vector_index("translate",1),
                   vector_index("translate",2));
}

DAEMatrix DAENode::rotation() {
  Matrix x(DAEVector(vector_index("rotate", "rotationX", 0),
                     vector_index("rotate", "rotationX", 1),
                     vector_index("rotate", "rotationX", 2)),
           Degrees(vector_index("rotate", "rotationX", 3)));
                                             
  Matrix y(DAEVector(vector_index("rotate", "rotationY", 0),
                     vector_index("rotate", "rotationY", 1),
                     vector_index("rotate", "rotationY", 2)),
           Degrees(vector_index("rotate", "rotationY", 3)));
                                             
  Matrix z(DAEVector(vector_index("rotate", "rotationZ", 0),
                     vector_index("rotate", "rotationZ", 1),
                     vector_index("rotate", "rotationZ", 2)),
           Degrees(vector_index("rotate", "rotationZ", 3)));
           
  DAEMatrix correction(DAEVector(1, 0, 0), Degrees(-90));
  return correction * x * y * z;
};

DAEGeometry DAENode::geometry() {
  TiXmlElement* instance_gemetry_element = element_->FirstChild("instance_geometry")->ToElement();
  std::string raw_url = instance_gemetry_element->Attribute("url");
  std::string url = DAEUtils::dae_id(raw_url);
  
  TiXmlElement* library_geometries_element = document_->RootElement()->FirstChild("library_geometries")->ToElement();
  for(TiXmlNode* child = library_geometries_element->FirstChild(); child != 0; child = child->NextSibling()) {
    TiXmlElement* child_element = child->ToElement();
    if (child_element->Attribute("id") == url) {
      return DAEGeometry(child_element, document_);
    }
  }
  
  LOG("DAE parsing error - couldnt match geometry to instance_geometry for", url.c_str());
  
  throw;
}
