#ifndef IINPUT_H_F9KFV1UE
#define IINPUT_H_F9KFV1UE

class IInput {
  
public:
  
  virtual void start_listening() = 0;
  virtual void update() = 0;
  
};


#endif /* end of include guard: IINPUT_H_F9KFV1UE */


