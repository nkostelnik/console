#ifndef JOYSTICK_BUTTON_H_5H2ILQLB
#define JOYSTICK_BUTTON_H_5H2ILQLB

#include "iinput_command.h"

#include <SDL/SDL.h>
  
class JoystickButton : public IInputCommand {
  
public:
  
  void execute(const SDL_Event& event);
  
};

#endif /* end of include guard: JOYSTICK_BUTTON_H_5H2ILQLB */
