#include "task_queue.h"

#include "log.h"

const int MAX_THREADS = 3;
TaskQueue::TaskDeque TaskQueue::queued_tasks_;
pthread_mutex_t TaskQueue::task_mutex_ = PTHREAD_MUTEX_INITIALIZER;

bool TaskQueue::working_ = false;
pthread_mutex_t TaskQueue::working_mutex_ = PTHREAD_MUTEX_INITIALIZER;

TaskQueue::ThreadDeque TaskQueue::threads_;

pthread_t* TaskQueue::spawn_thread(void*(*thread_function)(void*), void* args) {
  pthread_t* thread = new pthread_t();
  pthread_attr_t thread_attributes;
  pthread_attr_init(&thread_attributes);
  pthread_attr_setdetachstate(&thread_attributes, PTHREAD_CREATE_JOINABLE);
  pthread_create(thread, &thread_attributes, thread_function, args);
  pthread_attr_destroy(&thread_attributes);
  return thread;
}

void TaskQueue::queue_task(func* task_function, void* args) {
  Task* task = new Task(task_function, args);
  pthread_mutex_lock(&task_mutex_);
  queued_tasks_.push(task);
  pthread_mutex_unlock(&task_mutex_);
}

void TaskQueue::init() {
  working_ = true;
  for (int i = 0; i < MAX_THREADS; i++) {
    pthread_t* thread = spawn_thread(TaskQueue::thread_worker, (new int(i)));
    threads_.push_back(thread);
  }
}

ITask* TaskQueue::next_task() {
  ITask* task = 0;
  pthread_mutex_lock(&task_mutex_);
  if (!queued_tasks_.empty()) {
    task = queued_tasks_.front();
    queued_tasks_.pop();
  }
  pthread_mutex_unlock(&task_mutex_);
  return task;
}

void* TaskQueue::thread_worker(void* args) {
  LOG("task worker created")
  while(true) {
    ITask* task = next_task();
    if (task != 0) {
      task->run();
      delete task;
      task = 0;
    }
  }
  LOG("task worker destroyed")
  return 0;
}

bool TaskQueue::working() {
  bool working;
  pthread_mutex_lock(&working_mutex_);
  working = working_;
  pthread_mutex_unlock(&working_mutex_);
  return working;
}

void TaskQueue::stop_tasks() {
  pthread_mutex_lock(&working_mutex_);
  working_ = false;
  pthread_mutex_unlock(&working_mutex_);
  sleep(1);
  
  for(ThreadDeque::iterator i = threads_.begin(); i != threads_.end(); ++i) {
    delete (*i); 
  }
}
