#include "vector.h"
#include "matrix.h"

#include "maths.h"
#include <math.h>

Vector operator * (const Vector& v, const Matrix& m) {
  return Vector(
    v.x * m.m11 + v.y * m.m21 + v.z * m.m31,
    v.x * m.m12 + v.y * m.m22 + v.z * m.m32,
    v.x * m.m13 + v.y * m.m23 + v.z * m.m33,
    v.w
    ); 
}

std::ostream& operator << (std::ostream& stream, const Matrix& m)
{
  stream << m.m11 << " " << m.m12 << " " << m.m13 << " " << m.m14 << std::endl;
  stream << m.m21 << " " << m.m22 << " " << m.m23 << " " << m.m24 << std::endl;
  stream << m.m31 << " " << m.m32 << " " << m.m33 << " " << m.m34 << std::endl;
  stream << m.m41 << " " << m.m42 << " " << m.m43 << " " << m.m44 << std::endl;
  return stream;
}


Matrix Matrix::operator * (const Matrix& m) const {
  Vector m1(m11 * m.m11 + m12 * m.m12 + m13 * m.m13, 
            m11 * m.m21 + m12 * m.m22 + m13 * m.m23, 
            m11 * m.m31 + m12 * m.m32 + m13 * m.m33);
            
  Vector m2(m21 * m.m11 + m22 * m.m12 + m23 * m.m13, 
            m21 * m.m21 + m22 * m.m22 + m23 * m.m23, 
            m21 * m.m31 + m22 * m.m32 + m23 * m.m33);

  Vector m3(m31 * m.m11 + m32 * m.m12 + m33 * m.m13, 
            m31 * m.m21 + m32 * m.m22 + m33 * m.m23, 
            m31 * m.m31 + m32 * m.m32 + m33 * m.m33);
             
  return Matrix(m1, m2, m3);
}

Matrix::Matrix(const Vector& axis, Degrees degrees) {
  init(axis, degrees.radians());
}
 
Matrix::Matrix(const Vector& axis, float radians) {
  init(axis, radians);
}

void Matrix::init(const Vector& axis, float radians) {
  m11 = Maths::square((1 - cos(radians)) * axis.x) + cos(radians);
  m12 = (((1 - cos(radians)) * axis.x) * axis.y) + (sin(radians) * axis.z);
  m13 = (((1 - cos(radians)) * axis.x) * axis.y) - (sin(radians) * axis.y);
  
  m21 = (((1 - cos(radians)) * axis.x) * axis.y) - (sin(radians) * axis.z);
  m22 = Maths::square((1 - cos(radians)) * axis.y) + cos(radians);
  m23 = (((1 - cos(radians)) * axis.y) * axis.z) + (sin(radians) * axis.x);

  m31 = (((1 - cos(radians)) * axis.x) * axis.y) + (sin(radians) * axis.y);
  m32 = (((1 - cos(radians)) * axis.y) * axis.z) - (sin(radians) * axis.x);
  m33 = (Maths::square((1 - cos(radians)) * axis.z)) + cos(radians);
}

