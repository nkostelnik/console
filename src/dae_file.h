#ifndef __DAEFILE
#define __DAEFILE

#include "geometry.h"
#include "vector.h"
#include "matrix.h"

#include "tinyxml.h"
#include "string_utils.hpp"

#include "log.h"

#include <deque>
#include <map>

class DAEUtils {
  
  public:   
  
  static std::string dae_id(const std::string& url) {
    return url.substr(1, url.length() - 1); // remove the hash from the beginning
  }
};

class DAEVector : public Vector {

public:

  typedef std::vector<DAEVector> VertexList;

  DAEVector(float x_, float y_, float z_, float w_ = 1.0f)
    : Vector(x_, z_, y_, w_) {  }

};

class DAEMatrix : public Matrix {
  
public:
  
  DAEMatrix(const DAEVector& row1, const DAEVector& row2, const DAEVector& row3)
    : Matrix(row1, row2, row3) { };

  DAEMatrix(const DAEVector& axis, const Degrees& angle)
    : Matrix(axis, angle) { };
  
  DAEMatrix operator *(const Matrix& o) const {
    return DAEMatrix(
      DAEVector(m11 * o.m11 + m12 * o.m21 + m13 * o.m31, 
                m11 * o.m12 + m12 * o.m22 + m13 * o.m32, 
                m11 * o.m13 + m12 * o.m23 + m13 * o.m33),
    
      DAEVector(m21 * o.m11 + m22 * o.m21 + m23 * o.m31, 
                m21 * o.m12 + m22 * o.m22 + m23 * o.m32, 
                m21 * o.m13 + m22 * o.m23 + m23 * o.m33),
    
      DAEVector(m31 * o.m11 + m32 * o.m21 + m33 * o.m31, 
                m31 * o.m12 + m32 * o.m22 + m33 * o.m32, 
                m31 * o.m13 + m32 * o.m23 + m33 * o.m33));
    }
  
};

class DAEColor : public Color {

public:
  
  DAEColor(float  r_, float g_, float b_, float a_)
    : Color(r_, g_, b_, a_) { }
};

class DAEEffect {
  
  typedef std::deque<float> FloatDeque;
  typedef std::vector<std::string> StringList;
  
public:
  
  DAEEffect(TiXmlElement* element, TiXmlDocument* document)
    : element_(element), document_(document) { }
    
  DAEColor diffuse() {
    std::string color_string = element_
      ->FirstChild("profile_COMMON")
      ->FirstChild("technique")
      ->FirstChild("lambert")
      ->FirstChild("diffuse")
      ->FirstChild("color")
      ->ToElement()
      ->GetText();
      
    StringList colors_raw = StringUtils::split(color_string, " ");
    FloatDeque colors = StringUtils::s2f(colors_raw);
    return DAEColor(colors[0], colors[1], colors[2], colors[3]);
  }
    
private:
  
  TiXmlElement* element_;
  TiXmlDocument* document_;
  
};

class DAEMaterial {
  
public:
  
  DAEMaterial()
    : element_(0) { } 
  
  DAEMaterial(TiXmlElement* element, TiXmlDocument* document)
    : element_(element), document_(document) { }

  DAEEffect effect() {
    std::string instance_effect_url = element_
      ->FirstChild("instance_effect")
      ->ToElement()
      ->Attribute("url");
      
    std::string effect_id = DAEUtils::dae_id(instance_effect_url);
    
    TiXmlElement* library_effects_element = document_->RootElement()->FirstChild("library_effects")->ToElement();
    for(TiXmlNode* child = library_effects_element->FirstChild(); child != 0; child = child->NextSibling()) {
      TiXmlElement* child_element = child->ToElement();
      if (child_element->Attribute("id") == effect_id) {
        return DAEEffect(child_element, document_);
      }
    }
    
    LOG("DAE parsing error - couldnt match effect to instance_effect for", effect_id.c_str());
  
    throw;
  }
  
private:
  
  TiXmlElement* element_;
  TiXmlDocument* document_;
};

enum DAECoordType {
  DAE_VERTEX_COORDS,
  DAE_NORMAL_COORDS,
  DAE_ERROR_COORDS = 99
};

class DAESource {
  
  typedef std::deque<Vector> VectorDeque;
  
public:
  
  DAESource(TiXmlElement* element) 
    : element_(element) { };
  
  
  VectorDeque coords() {
    return VectorDeque();
//    FloatList all_indices = parse_indices();
//    FloatList indices;
//    
//    for(int i = 0, offset = 0; i < all_indices.size(); i++) {
//      if (offset == inputs().back().offset()) {
//        offset = 0; continue;
//      }
//      if (offset == inputs()[coord_type].offset()) {
//        indices.push_back(all_indices[i]);
//      }
//      offset++;
//    }
//    
//    FloatList positions = parse_positions();  
//    VectorDeque vertices;
//    
//    for(FloatList::iterator i = indices.begin(); i != indices.end(); ++i) {
//      int position_index = (*i) * 3;
//      
//      DAEVector vertex(positions[position_index], 
//                       positions[position_index + 1], 
//                       positions[position_index + 2]);
//      
//      vertices.push_back(vertex);
//    }
//    
//    return vertices;          
  }  
  
private:
  
  TiXmlElement* element_;
  
};

class DAEInput {

public:
  
  DAEInput() { };
  
  DAEInput(TiXmlElement* element) 
    : element_(element) { };
  
  unsigned int offset() {
    int offset = 0;
    element_->QueryIntAttribute("offset", &offset);
    return offset;
  }
  
  DAECoordType semantic() {
    std::string semantic = element_->Attribute("semantic");
    if (semantic.compare("VERTEX") == 0) {
      return DAE_VERTEX_COORDS; 
    }
    if (semantic.compare("NORMAL") == 0) {
      return DAE_NORMAL_COORDS; 
    }  
    return DAE_ERROR_COORDS;
  }
  
  DAESource source() {
    std::string source_url = element_->Attribute("source");
    std::string source_id = DAEUtils::dae_id(source_url);
    
    if (semantic() == DAE_VERTEX_COORDS) {
      TiXmlElement* verts_element = element_
        ->Parent()
        ->Parent()
        ->FirstChild("vertices")
        ->FirstChild()
        ->ToElement();
      
      return DAEInput(verts_element).source();
    }
    
    TiXmlElement* mesh_element = element_->Parent()->Parent()->ToElement();
    for(TiXmlNode* source = mesh_element->FirstChild("source"); source != 0; source = source->NextSibling("source")) {
      TiXmlElement* source_element = source->ToElement();
      std::string source_element_id = source_element->Attribute("id");
      if (source_element_id.compare(source_id) == 0) {
        return DAESource(source_element);
      }
    }
    
    LOG("DAE parsing error - couldnt match source to input for", source_url.c_str());
    
    throw;
    
  };
  
private:
  
  TiXmlElement* element_;
  
};

class DAEPolylist {

  typedef std::deque<float> FloatList;
  typedef std::vector<std::string> StringList;
  typedef std::deque<Vector> VectorDeque;
  typedef std::map<DAECoordType, DAEInput> InputMap;
  
public:

  DAEPolylist(TiXmlElement* element, TiXmlDocument* document)
    : element_(element)
    , document_(document) { };
  
  float* coords_array(DAECoordType coord_type) {
    return array(inputs()[coord_type].source().coords());
  }  
            
  unsigned int vertex_count() {
    return 0;
    //return coords(DAE_VERTEX_COORDS).size();
  }
  
  DAEMaterial material() {
    std::string url = element_
    ->Attribute("material");
    
    std::string material_id = url;//DAEUtils::dae_id(url);
    
    TiXmlElement* library_materials_element = document_->RootElement()->FirstChild("library_materials")->ToElement();
    for(TiXmlNode* child = library_materials_element->FirstChild(); child != 0; child = child->NextSibling()) {
      TiXmlElement* child_element = child->ToElement();
      if (child_element->Attribute("id") == url) {
        return DAEMaterial(child_element, document_);
      }
    }
    
    LOG("DAE parsing error - couldnt match material to instance_material for", url.c_str());
    
    throw;
  }
  
private:
  
  InputMap inputs() {
    InputMap inputs;
    for(TiXmlNode* child = element_->FirstChild("input"); child != 0; child = child->NextSibling("input")) {
      DAEInput input(child->ToElement());
      inputs[input.semantic()] = input;
    }
    return inputs;
  }
      
  float* array(const VectorDeque& vec) {
    int j = 0;
    float* float_array = new float[vec.size() * 3];
    for (VectorDeque::const_iterator i = vec.begin(); i != vec.end(); ++i) {
      float_array[j++] = (*i).x;
      float_array[j++] = (*i).y;
      float_array[j++] = (*i).z;
    }
    
    return float_array;
  }
    
private:
  
  TiXmlElement* element_;
  TiXmlDocument* document_;

};
  
class DAEMesh {
  
  typedef std::deque<DAEPolylist> PolylistDeque;
  
public:
  
  DAEMesh(TiXmlElement* element, TiXmlDocument* document)
    : element_(element)
    , document_(document) { };
    
  PolylistDeque polylists() {
    PolylistDeque polylists;
    
    for(TiXmlNode* child = element_->FirstChild("polylist"); child != 0; child = child->NextSibling("polylist")) {
      DAEPolylist polylist(child->ToElement(), document_);
      polylists.push_back(polylist);
    }
    
    return polylists;    
  }  
    
private:
    
  TiXmlElement* element_;
  TiXmlDocument* document_;
  
};

class DAEGeometry {
  
public:
  
  DAEGeometry(TiXmlElement* element, TiXmlDocument* document)
    : element_(element), document_(document) { }
      
  DAEMesh mesh() {
    return DAEMesh(element_->FirstChild("mesh")->ToElement(), document_);
  }
    
private:

  TiXmlElement* element_;
  TiXmlDocument* document_;  
    
};

class DAENode {
  
public:
  
  DAENode(TiXmlElement* element, TiXmlDocument* document)
    : element_(element)
    , document_(document) { }
    
  DAEGeometry geometry();
  
  DAEVector scale();
  DAEVector translation();
  DAEMatrix rotation();
    
private:
  
  float vector_index(const std::string& key, int index);
  float vector_index(const std::string& key, const std::string& sid, int index);
  std::string dae_id(const std::string& url);
    
  TiXmlElement* element_;
  TiXmlDocument* document_;
};

class DAEVisualScene {
  
  typedef std::deque<DAENode> DAENodeDeque;
  
public:
  
  DAEVisualScene(TiXmlElement* element, TiXmlDocument* document)
    : element_(element)
    , document_(document) { }
    
  DAENodeDeque nodes();
    
private:
  
  TiXmlElement* element_;
  TiXmlDocument* document_;
  
};

class DAEFile {
  
  typedef std::deque<DAEVisualScene> DAEVisualSceneDeque;

public:

  DAEFile() { };

  void load(const std::string& path) {
    if (document_.LoadFile(path.c_str())) {
      LOG("dae file loaded");
    }
    else {
      LOG("dae file failed to load");
    }    
  }
  
  DAEVisualSceneDeque scenes();

private:
  
  TiXmlDocument document_;
  DAEVisualSceneDeque scenes_;

};

class DAEFileAdapter {
  
  typedef std::deque<Geometry> GeometryDeque;
  
  public:
  
  DAEFileAdapter(DAEFile* file)
    : file_(file) { };
    
  GeometryDeque geometry() {
    GeometryDeque geometries;

    typedef std::deque<DAEVisualScene> VisualSceneDeque;
    typedef std::deque<DAENode> NodeDeque;
    
    VisualSceneDeque scenes = file_->scenes();
    for (VisualSceneDeque::iterator scene = scenes.begin(); scene != scenes.end(); ++scene) {
      
      NodeDeque nodes = (*scene).nodes();
      for (NodeDeque::iterator node = nodes.begin(); node != nodes.end(); ++node) {

        (*node).geometry();
        DAEPolylist polylist = (*node).geometry().mesh().polylists().front();
        
        Geometry geometry(polylist.coords_array(DAE_VERTEX_COORDS), 
                          polylist.coords_array(DAE_NORMAL_COORDS), 
                          polylist.vertex_count());
        
        geometry.rotate((*node).rotation());                      
        geometry.translate((*node).translation());
        geometry.scale((*node).scale());
        
        //Material material(polylist.material().effect().diffuse());
        //geometry.add_material(material);
        
        geometries.push_back(geometry);
      }
    }
    
    return geometries;
  }
  
  private:
  
  DAEFile* file_;
  
};

#endif
