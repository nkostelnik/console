#ifndef __LOG
#define __LOG

  #include <pthread.h>
  #include <iostream>
  #include <string>
  #include <sstream>

  class Log {
   
    static pthread_mutex_t* mutexsum_;
   
  public:
  
    Log();
    
    void log(const std::string& message) {
      pthread_mutex_lock(mutexsum_);
      std::clog << message << std::endl;
      pthread_mutex_unlock(mutexsum_);
    }

    template<class A>
    void log(A a) {
      std::stringstream message;
      message << a;
      log(message.str());
    }    
    
    template<class A, class B>
    void log(A a, B b) {
      std::stringstream message;
      message << a << " " << b;
      log(message.str());    
    }
    
  };

  #define LOG(...) { Log().log(__VA_ARGS__); }

#endif
