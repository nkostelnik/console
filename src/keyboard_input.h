#ifndef KEYBOARDINPUTCOMMAND_H_XTK3GX6I
#define KEYBOARDINPUTCOMMAND_H_XTK3GX6I

#include "iinput_command.h"

#include <SDL/SDL.h>
  
class KeyboardInput : public IInputCommand {
  
public:
  
  inline void execute(void* data) {
    this->execute(*static_cast<SDL_Event*>(data)); 
  }

  void execute(const SDL_Event& event);
  
};

#endif /* end of include guard: KEYBOARDINPUTCOMMAND_H_XTK3GX6I */
