#include "keyboard_input.h"

#include "log.h"
#include "game_state.h"

void KeyboardInput::execute(const SDL_Event& event) {
  
  if (event.key.keysym.sym == SDLK_q) {
    GameState::instance()->quit();
  }
  
};