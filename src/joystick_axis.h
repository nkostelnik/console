#ifndef JOYSTICK_AXIS_H_D2KTU957
#define JOYSTICK_AXIS_H_D2KTU957

#include "iinput_command.h"

#include <SDL/SDL.h>
  
class JoystickAxis : public IInputCommand {
  
public:
  
  void execute(const SDL_Event& event);
  
};


#endif /* end of include guard: JOYSTICK_AXIS_H_D2KTU957 */
