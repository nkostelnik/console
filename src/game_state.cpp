#include "game_state.h"

GameState* GameState::instance_ = 0;

void GameState::init() {
  instance_ = new GameState();
}

GameState* GameState::instance() { 
  return instance_;
}

void GameState::destroy() {
  delete instance_;
}
