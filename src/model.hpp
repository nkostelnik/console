#include "dae_file.h"
#include "geometry.h"

#include <vector>
#include <sstream>

class Model
{
  typedef std::vector<float> FloatList;
  typedef std::deque<Geometry> GeometryDeque;

public:

  void load(DAEFile* file) {
    geometry_ = DAEFileAdapter(file).geometry();
  }

  void render()
  {
    for (GeometryDeque::iterator i = geometry_.begin(); i != geometry_.end(); ++i) {
      (*i).render();
    }
  }
  
  private:
  
  GeometryDeque geometry_;

};
