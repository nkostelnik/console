#include "log.h"
#include "input.h"
#include "keyboard_input.h"
#include "joystick_button.h"
#include "joystick_axis.h"
#include "task_queue.h"

#include <SDL/SDL.h>

void Input::init() {
  SDL_JoystickOpen(0);
  commands_[SDL_KEYDOWN] = new KeyboardInput();
  commands_[SDL_JOYBUTTONDOWN] = new JoystickButton();
  commands_[SDL_JOYAXISMOTION] = new JoystickAxis();
};

void Input::destroy() { };

void Input::update() {
  
  SDL_Event event;  
  
  while (SDL_PollEvent(&event)) {      
    if (commands_.find(event.type) != commands_.end()) {      
      TaskQueue::queue_task(commands_[event.type], &IInputCommand::task, new SDL_Event(event));      
    }
  }
}
