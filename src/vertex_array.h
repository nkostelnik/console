#ifndef __VERTEXARRAY
#define __VERTEXARRAY

#include "vector.h"

struct VertexArray {
  Vector* vertices;
  int size;
}

#endif
