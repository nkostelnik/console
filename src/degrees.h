#ifndef DEGREES_H_4M0HG7PQ
#define DEGREES_H_4M0HG7PQ

class PI {

public:
  
  static float value() { 
    return 3.14159265;
  }

  
};

class Degrees {
  
public:
  
  Degrees(float degrees) 
    : degrees_(degrees) { };
    
  float radians() {
    return (degrees_ * PI::value()) / 180.0f;
  }
  
private:
  
  float degrees_;

};

class Radians {
  
public:
  
  Radians(float radians) 
    : radians_(radians) { };
    
  float degrees() {
    return radians_ * (PI::value() / 180.0f);
  }
  
private:
  
  float radians_;

};

#endif /* end of include guard: DEGREES_H_4M0HG7PQ */
