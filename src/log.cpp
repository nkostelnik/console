#include "log.h"

#include <sstream>
#include <stdarg.h>

pthread_mutex_t* Log::mutexsum_ = 0;

Log::Log() {
  if (mutexsum_ == 0) {
    mutexsum_ = new pthread_mutex_t();
  }
}

